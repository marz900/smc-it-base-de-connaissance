import sqlite3
import pandas as pd
from xlrd import open_workbook
import glob
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.units import inch
from reportlab.platypus import Table, TableStyle, Paragraph, SimpleDocTemplate
from reportlab.lib.styles import getSampleStyleSheet


# Chemin vers les fichiers Excel
chemin = "C:/Users/Fanta/Desktop/python_react_App/fichiers_excels/*.xlsx"

# Lire les fichiers Excel en bouclant sur les noms de fichier
df_list = []
for nom_fichier in glob.glob(chemin):
    df = pd.read_excel(nom_fichier, sheet_name="BD")
    df_list.append(df)

# Concaténer les DataFrames en un seul DataFrame
df_concat = pd.concat(df_list)

# Convertir la colonne "DATE" en un objet datetime avec le format jj/mm/aaaa
df_concat["DATE"] = pd.to_datetime(df_concat["DATE"], format="%d/%m/%Y")

# Formater les dates selon le format souhaité
df_concat["DATE"] = df_concat["DATE"].apply(lambda x: x.strftime("%d/%m/%Y"))

if not df_concat['Disponibilite'].empty :
     try:
        # Convertir la colonne "disponibilite" en pourcentage avec 2 chiffres après la virgule
        df_concat["Disponibilite"] = df_concat["Disponibilite"].apply(lambda x: '{:.2%}'.format(x))
     except:
         print("Erreur lors de la conversion de la colonne 'Disponibilite'")

if not df_concat['DUREE'].empty :
     try:
        # Convertir la colonne "DUREE" en un objet timedelta avec le format hh:mm:ss
        df_concat["DUREE"] = pd.to_datetime(df_concat["DUREE"], format='%H:%M:%S', errors='coerce')
        df_concat["DUREE"] = df_concat["DUREE"].apply(lambda x: x.strftime("%H:%M:%S") if not pd.isnull(x) else None)
     except:
         print("Erreur lors de la conversion de la colonne 'DUREE'")

# Connexion à la base de données SQLite
conn = sqlite3.connect("MaBase.db")

# Création d’une table
cursor = conn.cursor()

cursor.execute("""
CREATE TABLE IF NOT EXISTS data(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    DATE DATE,
    SEMAINE WEEK,
    APPLICATION TEXT,
    DUREE TIME,
    Duree_de_Travail_Semaine TEXT,
    Disponibilite FLOAT,
    Seuil TEXT,
    IMPACT TEXT,
    CAUSE TEXT,
    SOURCE_DE_LA_CAUSE TEXT,
    SOLUTION TEXT,
    FACTEUR TEXT,
    TICKET INTEGER,
    Fiche TEXT,
    point_suivi TEXT,
    solution_definitive TEXT,
    service TEXT,
    annee YEAR,
    type TEXT
);
""")

# Insertion des données dans la table
df_concat.to_sql("data", conn, if_exists="append", index=False)

# Création de la table "users"
conn.execute('''CREATE TABLE users
             (id INTEGER PRIMARY KEY AUTOINCREMENT,
             username TEXT NOT NULL,
             password TEXT NOT NULL);''')

login_admin = "Koté Fanta"
password_admin = "12345"
cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (login_admin, password_admin))
conn.commit()
    # Enregistrer les modifications et fermer la connexion à la base de données
cursor.execute('''SELECT * FROM users''')
a = cursor.fetchall()
print(a)

conn = sqlite3.connect('MaBase.db')
cursor = conn.cursor()

# Récupération de toutes les données
cursor.execute('''SELECT * FROM data''')
base = cursor.fetchall()

# Initialisation du dictionnaire des données
data = {}

# Récupération des données pour chaque application
for row in base:
    app = row[3]
    week = row[2]
    availability = row[6]
    
    if app not in data:
        # Création d'une nouvelle entrée pour l'application
        data[app] = {week: {'count': 1, 'availability': availability}}
    else:
        if week in data[app]:
            # Incrément du compteur et ajout de la disponibilité pour cette semaine
            data[app][week]['count'] += 1
            
        else:
            # Ajout d'une nouvelle entrée pour cette semaine
            data[app][week] = {'count': 1, 'availability': availability}

# Affichage des données pour chaque application
for app, app_data in data.items():
    print(f"Application: {app}")
    print("Semaine\tNombre d'apparitions\tTaux de disponibilité")
    for week, week_data in app_data.items():
        count = week_data['count']
        availability = week_data['availability']
        print(f"{week}\t{count}\t{availability}")
    print("----------------------")

# Récupération des données de la ligne souhaitée

conn.close()