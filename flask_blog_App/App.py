from flask import Flask, render_template, request, redirect, url_for, send_from_directory, session, send_file
import sqlite3
import datetime
import os
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Paragraph, SimpleDocTemplate, Spacer
from dateutil import parser


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'uploads/'


#conn = sqlite3.connect('MaBase.db')
# Création de la table "users"
#conn.execute('''CREATE TABLE users
 #            (id INTEGER PRIMARY KEY AUTOINCREMENT,
 #            username TEXT NOT NULL,
 #            password TEXT NOT NULL);''')
# Define a function to validate user credentials
#cursor = conn.cursor()

# Insérer des logins "admin" dans la table "users"
#login_admin = "admin"
#password_admin = "12345"
#cursor.execute("INSERT INTO users (username, password) VALUES (?, ?)", (login_admin, password_admin))

#conn.close()

@app.route('/login')
def connexion():
    return render_template('login.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Handle the login form submission
        username = request.form['username']
        password = request.form['password']
        conn = sqlite3.connect('MaBase.db')
        c = conn.cursor()
        # Perform user authentication
        c.execute("SELECT * FROM users WHERE username = ? AND password = ?", (username, password,))
        user = c.fetchone()
        conn.close()
        if user:
            # If authentication succeeds, redirect to the home page
            return render_template('add.html', user = user)
        else:
            return False

        # ...
        
    # If the request method is GET, display the login form
    return render_template('login.html')

###
#Page d'accueil (index.html) affichage simple 
###


@app.route('/')
def index():
   return render_template('index.html')

###
#Page d'affichage base de connaissance tableau (base.html)
###

@app.route('/base')
def base():
    conn = sqlite3.connect('MaBase.db')
    c = conn.cursor()

    c.execute("SELECT * FROM data")
    data = c.fetchall()
    
    for a in data :
        file_name = a[14] 
    # Récupération des données triées par ordre chronologique
    c.execute("SELECT * FROM data ORDER BY id DESC")
    rows = c.fetchall()

    # Création d'un dictionnaire pour stocker les données par année
    data_by_year = {}

    # Boucle sur les données pour ranger les entrées dans le dictionnaire par année
    for row in rows:
        year = row[1][6:] # Récupération de l'année de la date
        if year not in data_by_year:
            data_by_year[year] = []
        data_by_year[year].append(row)

    # Fermeture de la connexion à la base de données
    conn.close()

    # Rendu du template HTML avec les données triées par année
    return render_template('base.html', data_by_year=data_by_year, file_name=file_name)


    
###
#Barre de recherche les recherche dans la base de donnée et afficher le resultat dans le tableau (Rechercher)
###

@app.route('/search', methods=['POST'])
def search():
    # Get the search query from the form
    query = request.form['query']

    # Connect to SQLite database
    conn = sqlite3.connect('MaBase.db')
    c = conn.cursor()

    # Define the base SQL query
    c.execute("SELECT * FROM data WHERE application LIKE ? or ticket LIKE ? or semaine LIKE ? or date LIKE ? ORDER BY id DESC", ('%'+query+'%', '%'+query+'%', '%'+query+'%', '%'+query+'%' ))
    rows = c.fetchall()

    # Create a dictionary to store the data by year
    data_by_year = {}

    # Loop over the rows to store the entries in the dictionary by year
    for row in rows:
        year = row[1][6:]  # Get the year from the date
        if year not in data_by_year:
            data_by_year[year] = []
        data_by_year[year].append(row)
        
    # Insert the new entry at the beginning of the list to maintain descending order
       # data_by_year[year].insert(0, row)
    # Close the connection to the database
    conn.close()

    # Render the HTML template with the data sorted by year
    return render_template('base.html', data_by_year=data_by_year)



###
#Formulaire pour ajouter un incident dans la base de données (add.html)
###

@app.route('/modif')
def modif():
    return render_template('modifier_ligne.html')


@app.route('/formulaire')
def formulaire():
    return render_template('add1.html')

@app.route('/add', methods=['POST', 'GET'])
def add():

    conn = sqlite3.connect('MaBase.db')

    date = request.form['date']

    date_obj = datetime.datetime.strptime(date, '%Y-%m-%d')
    date = date_obj.strftime("%d/%m/%Y")

# Obtenir le numéro de semaine à partir de la date
    week_num = date_obj.isocalendar()[1]

# Afficher le numéro de semaine
    semaine = "{}".format(week_num)
    source_cause = request.form['source_cause']
    cause = request.form['cause']
    impact = request.form['impact']
    ticket = request.form['ticket']
    heure_debut = request.form['date_heure_debut']
    heure_fin = request.form['date_heure_fin']
    date_heure_debut = parser.parse(heure_debut)
    date_heure_fin = parser.parse(heure_fin)
    duree = date_heure_fin - date_heure_debut
    heures, minutes = divmod(duree.seconds // 60, 60)
    duree = str(duree)
    try:
        heures, minutes = map(int, duree.split(":"))
    except ValueError:
        print("La durée n'est pas au format attendu")
    total_secondes1 = heures * 3600 + minutes * 60

    application = request.form['application']
    Duree_de_Travail_Semaine = "105:00:00"
    solution = request.form['solution']
    facteur = ' '
    heures, minutes2, secondes2 = 0, 0, 0
    try:
        heures2, minutes2, secondes2 = map(int, Duree_de_Travail_Semaine.split(":"))
    except ValueError:
        print("La duree de travail n'est pas au format attendu")
    total_secondes2 = heures2 * 3600 + minutes2 * 60 + secondes2 
    a = (1-total_secondes1/total_secondes2)*100
    disponibilite = "{:.2f}".format(a)
    seuil = '99,6%'
    fiche = request.files['fiche']
    file_name = fiche.filename
    point_suivi = request.form.get('point_suivi')
    solution_definitive = request.form['solution_definitive']
    service = request.form['service']
    annee= date_obj.year
    Type = request.form['type']
    
    # Connect to SQLite database
    c = conn.cursor()
    
    c.execute("INSERT INTO data (DATE, SEMAINE, APPLICATION, DUREE, Duree_de_Travail_Semaine, Disponibilite, Seuil, IMPACT, CAUSE, SOURCE_DE_LA_CAUSE, SOLUTION, FACTEUR, TICKET, Fiche, point_suivi, solution_definitive, service, annee, type ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (date, semaine, application, duree, Duree_de_Travail_Semaine, disponibilite, seuil, impact, cause, source_cause, solution, facteur, ticket, file_name, point_suivi, solution_definitive, service, annee, Type))
    
    conn.commit()
    conn.close()
    
    succes = 'Incident renseigné dans la base de connaissance avec succes'
    return render_template('add.html')


    
@app.route('/download', methods=['POST', 'GET'])
def download():
    conn = sqlite3.connect('MaBase.db')
    cursor = conn.cursor()

    ticket = request.form['ticket']
    cursor.execute("SELECT * FROM data WHERE TICKET=?", (ticket,))
    row = cursor.fetchone()

    styles = getSampleStyleSheet()
    heading_style = styles['Heading1']
    normal_style = styles['Normal']
    line_break_style = ParagraphStyle('LineBreakStyle', parent=normal_style, spaceAfter=12, wordWrap='CJK')

    data1 = [
        ['ID:', str(row[0])],
        ['Date:', str(row[1])],
        ['Semaine:', str(row[2])],
        ['Application:', str(row[3])],
        ['Durée:', str(row[4])],
    ]

    if row[16] is None:
        data2 = [
            ['Impact:', str(row[8])],
            ['Cause:', str(row[9])],
            ['Source de la cause:', str(row[10])],
            ['Solution temporaire:', str(row[11])],
            ['Numero de ticket:', str(row[13])]
        ]
    else:
        data2 = [
            ['Impact:', str(row[8])],
            ['Cause:', str(row[9])],
            ['Source de la cause:', str(row[10])],
            ['Solution temporaire:', str(row[11])],
            ['Solution définitive', str(row[16])],
            ['Numero de ticket:', str(row[13])]
        ]

    if not os.path.exists("upload"):
        os.makedirs("upload")

    file_path = os.path.join("upload", str(row[13]) + ".pdf")
    if os.path.exists(file_path):
        os.remove(file_path)

    pdf = canvas.Canvas(file_path, pagesize=letter)

    if data1:
        pdf.setFont("Helvetica-Bold", 14)
        pdf.drawString(100, 700, "Tableau 1")
        pdf.setFont("Helvetica", 12)
        y = 680
        for entry in data1:
            pdf.drawString(100, y, entry[0])
            pdf.drawString(250, y, entry[1])
            y -= 20

    if data2:
        pdf.setFont("Helvetica-Bold", 14)
        pdf.drawString(100, 700, " ")
        pdf.setFont("Helvetica", 12)
        y = 580
        for entry in data2:
            pdf.drawString(100, y, entry[0])
            pdf.drawString(250, y, entry[1])
            y -= 20

    pdf.save()

    conn.close()

    return send_file(file_path, as_attachment=True)



@app.route('/index1')
def index1():
    # Connexion à la base de données
    conn = sqlite3.connect('MaBase.db')
    c = conn.cursor()

    # Récupération des données à partir de la base de données
    c.execute('SELECT * FROM data')
    data = c.fetchall()

    # Création de la courbe à partir des données

    x = [row[0] for row in data]
    y = [row[2] for row in data]
    
    plt.plot(x, y)
    plt.savefig('static/images/plot.png')

    # Affichage de la page avec la courbe
    return render_template('index1.html')

if __name__ == '__main__':
    app.run(debug=True)
