import datetime

duree1 = "20:55:01"
heures1, minutes1, secondes1 = map(int, duree1.split(":"))
total_secondes1 = heures1 * 3600 + minutes1 * 60 + secondes1
print(total_secondes1)

#route pour la suppression d'une ligne de la base de données depuis un bouton de la ligne du tableau html


@app.route('/delete_row', methods=['POST'])
def delete_row():
    # Récupérer les données envoyées dans la requête
    row_id = request.form['row_id']

    # Se connecter à la base de données
    conn = sqlite3.connect('MaBase.db')
    c = conn.cursor()

    # Exécuter la requête SQL pour supprimer la ligne de la table
    query = "DELETE FROM data WHERE id = {}".format(row_id)
    c.execute(query)

    # Enregistrer les modifications dans la base de données
    conn.commit()
    conn.close()

    # Retourner une réponse à l'utilisateur
    return "La ligne avec l'ID {} a été supprimée de la base de données".format(row_id)